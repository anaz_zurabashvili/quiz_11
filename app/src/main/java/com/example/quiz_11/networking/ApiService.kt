package com.example.quiz_11.networking

import com.example.quiz_11.ui.notifications.models.Notification
import com.example.quiz_11.utils.Constants
import retrofit2.Response
import retrofit2.http.GET


interface ApiService {

    @GET(Constants.NOTIFICATIONS)
    suspend fun getNotificationData(): Response<List<Notification>>
}
