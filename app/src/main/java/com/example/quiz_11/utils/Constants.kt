package com.example.quiz_11.utils

object Constants {
    const val BASE_URL = "https://run.mocky.io/v3/"
    const val NOTIFICATIONS = "80d25aee-d9a6-4e9c-b1d1-80d2a7c979bf"
}