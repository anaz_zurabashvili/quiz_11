package com.example.quiz_11.ui.notifications.vms

import com.example.quiz_11.networking.ApiService
import com.example.quiz_11.ui.notifications.models.Notification
import com.example.quiz_11.utils.Resource
import com.example.quiz_11.utils.handleResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class NotificationsRepository @Inject constructor(private val api: ApiService) {

    suspend fun getNotifications(): Flow<Resource<List<Notification>>> {
        return flow {
            emit(handleResponse { api.getNotificationData() })
        }.flowOn(Dispatchers.IO)
    }
}