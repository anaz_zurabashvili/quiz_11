package com.example.quiz_11.ui.notifications.models

data class UiState(
    val isLoading: Boolean = true ,
    val error: Error? = null ,
    val notifications: List<Notification> = listOf() ,
) {
    enum class Error(val message: String) {
        API_ERROR("Network Error"),
    }
}