package com.example.quiz_11.ui.notifications.vms

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.quiz_11.ui.notifications.models.UiState
import com.example.quiz_11.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(private val repository: NotificationsRepository) :
    ViewModel() {

    private val _uiState: MutableStateFlow<UiState> = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState

    fun getNotificationsResponse() = viewModelScope.launch {
        repository.getNotifications().collectLatest { value->
            when (value) {
                is Resource.Success<*> -> {
                    _uiState.value =  value.data?.let { UiState(notifications = value.data, isLoading = false) }!!
                }
                is Resource.Error<*> -> {
                    _uiState.value = UiState(error = UiState.Error.API_ERROR, isLoading = false)
                }
            }
        }
    }
}

