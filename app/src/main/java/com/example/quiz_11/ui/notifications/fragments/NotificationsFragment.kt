package com.example.quiz_11.ui.notifications.fragments


import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz_11.ui.base.BaseFragment
import com.example.quiz_11.databinding.FragmentNotificationsBinding
import com.example.quiz_11.utils.extensions.STRINGS
import com.example.quiz_11.utils.extensions.gone
import com.example.quiz_11.utils.extensions.visible
import com.example.quiz_11.ui.notifications.vms.NotificationViewModel
import com.example.quiz_11.ui.notifications.adapters.NotificationsAdapter
import com.example.quiz_11.ui.notifications.models.Notification
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NotificationsFragment :
    BaseFragment<FragmentNotificationsBinding>(FragmentNotificationsBinding::inflate) {
    private lateinit var notificationsAdapter: NotificationsAdapter
    private val viewModel: NotificationViewModel by viewModels()

    override fun init() {
        getResponse()
        initRV()
        observer()
    }

    private fun getResponse() {
        viewModel.getNotificationsResponse()
    }

    private fun initRV() {
        binding.rvNotifications.apply {
            notificationsAdapter = NotificationsAdapter()
            adapter = notificationsAdapter
            layoutManager =
                LinearLayoutManager(view?.context , LinearLayoutManager.VERTICAL , false)
        }
    }

    private fun observer() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.uiState.collectLatest { state ->
                state.notifications?.let {
                    getStatus(it , true)
                }
                state.error?.let {
                    getStatus(null , false)
                }
            }
        }
    }

    private fun getStatus(data: List<Notification>? , state: Boolean) {
        with(binding) {
            if (state) {
                tvStatus.gone()
                notificationsAdapter.setData(data!!)
            } else {
                tvStatus.visible()
                tvStatus.text = getString(STRINGS.notifications_is_empty)
            }
        }

    }
}