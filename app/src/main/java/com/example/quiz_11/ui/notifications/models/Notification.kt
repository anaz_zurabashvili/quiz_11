package com.example.quiz_11.ui.notifications.models


import com.squareup.moshi.Json

data class Notification(
    val avatar: String?,
    val email: String?,
    @Json(name = "first_name")
    val firstName: String?,
    val id: Int?,
    @Json(name = "is_typing")
    val isTyping: Boolean?,
    @Json(name = "last_message")
    val lastMessage: String?,
    @Json(name = "last_name")
    val lastName: String?,
    @Json(name = "message_type")
    val messageType: String?,
    @Json(name = "unrea_message")
    val unreaMessage: Int?,
    @Json(name = "updated_date")
    val updatedDate: Long?
)

enum class MessageType{
    TEXT, ATTACHMENT
}