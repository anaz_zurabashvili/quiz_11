package com.example.quiz_11.ui.notifications.adapters

import android.util.Log.d
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz_11.databinding.LayoutNotificationBinding
import com.example.quiz_11.utils.extensions.setImageUrl
import com.example.quiz_11.ui.notifications.models.MessageType
import com.example.quiz_11.ui.notifications.models.Notification
import com.example.quiz_11.utils.extensions.getTime
import com.example.quiz_11.utils.extensions.gone
import com.example.quiz_11.utils.extensions.visible


class NotificationsAdapter() : RecyclerView.Adapter<NotificationsAdapter.ViewHolder>() {

    private var notifications = mutableListOf<Notification>()

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int) =
        ViewHolder(
            LayoutNotificationBinding.inflate(
                LayoutInflater.from(parent.context) ,
                parent ,
                false
            )
        )

    override fun onBindViewHolder(
        holder: ViewHolder ,
        position: Int
    ) =
        holder.onBind()

    inner class ViewHolder(private val binding: LayoutNotificationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private lateinit var model: Notification
        fun onBind() {
            model = notifications[adapterPosition]
            binding.apply {
                imAvatar.setImageUrl(model.avatar)
                tvName.text = model.firstName.plus(" ").plus(model.lastName)
                if (model.messageType == MessageType.TEXT.toString().lowercase()) {
                    tvMessage.text = model.lastMessage
                    model.lastMessage.let { tvMessage.text = it }
                } else {
                    tvMessage.text = "Sent an attachment"
                }
                if (model.isTyping == true) tvTyping.visible() else tvTyping.gone()
                if (model.unreaMessage != 0) {
                    tvUnreadMessage.text = model.unreaMessage.toString()
                    tvUnreadMessage.visible()
                } else tvUnreadMessage.gone()
                if (model.updatedDate !=null){
                    d("time" , "${model.updatedDate?.getTime()}")
                }
            }

        }
    }

    override fun getItemCount() = notifications.size

    fun setData(notifications: List<Notification>) {
        this.notifications = notifications.toMutableList()
        notifyDataSetChanged()
    }
}